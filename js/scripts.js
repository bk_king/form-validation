(function () {

    var form = document.querySelector("#myForm"),
            fields = form.querySelectorAll("[data-error]");
    form.addEventListener('submit', validate);

    function isNotEmpty(field) {
        return field !== '';
    }
    function isCorrectEmail(field) {
        return field.indexOf('@') === -1 ? false : true;
    }
    function checkLength(field, min) {
        return field.length >= min ? true : false;
    }
    function displayError(array) {

        var ul = document.querySelector('ul.errors'),
                i,
                length = array.length;

        if (ul === null) {
            ul = document.createElement('ul');
            ul.classList.add('errors');
        }
        ul.innerHTML = '';
        for (i = 0; i < length; i++) {
            var li = document.createElement('li');
            li.textContent = array[i];
            ul.appendChild(li);
        }
        form.parentNode.insertBefore(ul, form);
    }
    function validate(e) {

        e.preventDefault();
        var i,
                length = fields.length,
                flag = true,
                errors = [];
        for (i = 0; i < length; i++) {

            if (fields[i].type === 'text') {
                flag = isNotEmpty(fields[i].value);
            } else if (fields[i].type === 'email') {
                flag = isCorrectEmail(fields[i].value);
            } else if (fields[i].type === 'select-one') {
                flag = isNotEmpty(fields[i].value);
            } else if (fields[i].type === 'textarea') {
                flag = checkLength(fields[i].value, 3);
            }
            if (flag !== true) {
                errors.push(fields[i].dataset.error);
                fields[i].classList.add('error');
            }else{
               fields[i].classList.remove('error'); 
            }
        }
        if (errors.length > 0) {
            displayError(errors);
        } else {
            form.submit();
        }

    }


})();